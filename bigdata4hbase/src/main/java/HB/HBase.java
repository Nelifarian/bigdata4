/**
 * Code from : https://autofei.wordpress.com/2012/04/02/java-example-code-using-hbase-data-model-operations/
 */
package HB;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HBase {

    private static Configuration conf = null;

    static {
        conf = HBaseConfiguration.create();
        conf.addResource(new Path("/etc/hbase/conf/hbase-site.xml"));
    }

    public static byte[] serializeArray(String[] arr)
    {
        Writable[] content = new Writable[arr.length];
        for (int i = 0; i < content.length; i++) {
            content[i] = new Text(arr[i]);
        }
        return WritableUtils.toByteArray(new ArrayWritable(Text.class, content));
    }

    public static String[] deSerializeArray(byte[] bytes) {
        ArrayWritable w;
        String[] arr = null;
        DataInputStream dataIn = null;
        try {
            w = new ArrayWritable(Text.class);
            ByteArrayInputStream in = new ByteArrayInputStream(bytes);
            dataIn = new DataInputStream(in);
            w.readFields(dataIn);
            Writable[] writables = w.get();
            arr = new String[writables.length];
            for (int i = 0; i < writables.length; i++) {
                arr[i] = writables[i].toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(dataIn);
        }

        return arr;
    }

    public static void createTable(String tableName, String[] familys)
            throws Exception {
        HBaseAdmin admin = new HBaseAdmin(conf);
        if (admin.tableExists(tableName)) {
            System.out.println("table already exists!");
        } else {
            HTableDescriptor tableDesc = new HTableDescriptor(tableName);
            for (int i = 0; i < familys.length; i++) {
                tableDesc.addFamily(new HColumnDescriptor(familys[i]));
            }
            admin.createTable(tableDesc);
            System.out.println("create table " + tableName + " ok.");
        }
    }

    public static void addRecord(String tableName, String rowKey,
                                 String family, String qualifier, String value) throws Exception {
        try {
            HTable table = new HTable(conf, tableName);
            Put put = new Put(Bytes.toBytes(rowKey));
            put.add(Bytes.toBytes(family), Bytes.toBytes(qualifier), Bytes
                    .toBytes(value));
            table.put(put);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addArray(String tableName, String rowKey,
                                 String family, String qualifier, String[] value) throws Exception {
        try {
            HTable table = new HTable(conf, tableName);
            Put put = new Put(Bytes.toBytes(rowKey));
            put.add(Bytes.toBytes(family), Bytes.toBytes(qualifier), serializeArray(value));
            table.put(put);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void delRecord(String tableName, String rowKey)
            throws IOException {
        HTable table = new HTable(conf, tableName);
        List<Delete> list = new ArrayList<Delete>();
        Delete del = new Delete(rowKey.getBytes());
        list.add(del);
        table.delete(list);
        System.out.println("del recorded " + rowKey + " ok.");
    }

    public static void getAllRecord (String tableName) {
        String family;
        try{
            HTable table = new HTable(conf, tableName);
            Scan scan = new Scan();
            ResultScanner results = table.getScanner(scan);
            for(Result result:results){
                for(KeyValue kv : result.raw()){
                    family = new String(kv.getQualifier());

                    System.out.print(new String(kv.getRow()) + " ");
                    System.out.print(new String(kv.getFamily()) + ":");
                    System.out.print(new String(kv.getQualifier()) + " ");
                    System.out.print(kv.getTimestamp() + " ");
                    if(family.equals("others")){
                        String[] friends = deSerializeArray(kv.getValue());
                        for(String friend : friends){
                            System.out.print("[");
                            System.out.print(friend + " ");
                            System.out.println("] ");
                        }
                    }else {
                        System.out.println(new String(kv.getValue()));
                    }
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String command;
        String name, bff, age, genre, address;
        String[] friends;
        try {
            String tableName = "vbhbase";
            String[] families = { "info", "friends" };
            HBase.createTable(tableName, families);
            do{
                command = "";
                System.out.println("all - Montre tout les éléments");
                System.out.println("new - Ajoute une personne dans la satabase");
                System.out.println("quit - Quitter");
                System.out.print("> ");
                command = scan.nextLine();
                if(command.equals("all")){
                    HBase.getAllRecord(tableName);
                }else if(command.equals("new")){
                    name = "";
                    System.out.println("Entrez le nom :");
                    name = scan.nextLine();
                    age ="";
                    System.out.print("l'age: ");
                    age = scan.nextLine();
                    genre = "";
                    System.out.print("le genre: ");
                    genre = scan.nextLine();
                    address = "";
                    System.out.print("et l'address: ");
                    address = scan.nextLine();
                    do {
                        bff = "";
                        System.out.println("Qui est votre meilleur ami ?");
                        bff = scan.nextLine();
                    }while(bff != "");
                    System.out.println("Saisissez les noms des amis, séparés par des espaces");
                    friends = scan.nextLine().split(" ");
                    HBase.addRecord(tableName, name, "info", "age", age);
                    HBase.addRecord(tableName, name, "info", "genre", genre);
                    HBase.addRecord(tableName, name, "info", "address", address);
                    HBase.addRecord(tableName, name, "friends", "bff", bff);
                    HBase.addArray(tableName, name, "friends", "others", friends);
                }
            }while(!command.equals("quit"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
